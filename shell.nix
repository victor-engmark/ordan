{ pkgs ? import
    (
      fetchTarball {
        name = "21.11-2022-02-02";
        url = "https://github.com/NixOS/nixpkgs/archive/02158e3fa439f58254fe8cda1b1ff50d93fa65cf.tar.gz";
        sha256 = "17yryy6rk1rr1dwawlhb448gb2jicbnswsp8rn5ll0jrn8dq9rlm";
      })
    { }
}:
pkgs.mkShell {
  buildInputs = [
    pkgs.black
    pkgs.gitFull
    pkgs.gitlint
    pkgs.mypy
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.python3Packages.coverage
    pkgs.python3Packages.isort
    pkgs.python3Packages.mutmut
    pkgs.python3Packages.pylint
    pkgs.python3Packages.pytest
    pkgs.python3Packages.pytest-randomly
    pkgs.shellcheck
  ];
}
