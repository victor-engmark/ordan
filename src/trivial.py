from typing import Literal


def trivial() -> Literal[True]:
    return True
