#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset

mutmut run --no-progress ${@+"$@"} || exit_code="$?"
mutmut junitxml > mutmut.xml

exit "${exit_code-0}"
