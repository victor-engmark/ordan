#!/usr/bin/env bash

set -o errexit

coverage run --module pytest --junitxml=report.xml

set +o errexit

coverage report
coverage xml
