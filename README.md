# ordan

Wordle-like game in the command line.

## Play

TODO, see issue [#5](https://gitlab.com/victor-engmark/ordan/-/issues/5) and
[#6](https://gitlab.com/victor-engmark/ordan/-/issues/6).

## Support

Please check [existing issues](https://gitlab.com/victor-engmark/ordan/-/issues)
before [filing a new one](https://gitlab.com/victor-engmark/ordan/-/issues/new).

## Contributing

Contributions are welcome. Make sure to coordinate via
[issues](https://gitlab.com/victor-engmark/ordan/-/issues) to avoid crossed
wires and to make sure we agree something in worthwhile before spending time on
it. Or [fork](https://gitlab.com/victor-engmark/ordan/-/forks/new) and go nuts!
😉

Your first step should probably be to
[install Nix, the package manager](https://nixos.org/download.html), then try
running some of the commands in the [GitLab CI configuration](.gitlab-ci.yml).

## Acknowledgments

Thank you to the Nix community, for helping to answer newbie questions like
mine.

## License

Copyright © 2022 Victor Engmark

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.

## Project status

Pre-alpha. There's nothing to see here yet.
